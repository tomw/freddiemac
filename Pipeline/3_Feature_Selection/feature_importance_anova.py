
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectKBest
import matplotlib.pyplot as plt
import seaborn as sns
PATH_TO_PIPELINE = "C:/Users/mot99/OneDrive/Dokumente/GitHub/freddiemac/Pipeline/"
data = pd.read_csv(PATH_TO_PIPELINE + r'3_Feature_Selection\encod_dataset_2010_20_eval.csv')
data = data.dropna()

states = ['Property State_AL', 'Property State_AR', 'Property State_AZ',
                'Property State_CA', 'Property State_CO', 'Property State_CT',
                'Property State_DC', 'Property State_DE', 'Property State_FL',
                'Property State_GA', 'Property State_HI', 'Property State_IA',
                'Property State_ID', 'Property State_IL', 'Property State_IN',
                'Property State_KS', 'Property State_KY', 'Property State_LA',
                'Property State_MA', 'Property State_MD', 'Property State_ME',
                'Property State_MI', 'Property State_MN', 'Property State_MO',
                'Property State_MS', 'Property State_MT', 'Property State_NC',
                'Property State_ND', 'Property State_NE', 'Property State_NH',
                'Property State_NJ', 'Property State_NM', 'Property State_NV',
                'Property State_NY', 'Property State_OH', 'Property State_OK',
                'Property State_OR', 'Property State_PA', 'Property State_PR',
                'Property State_RI', 'Property State_SC', 'Property State_SD',
                'Property State_TN', 'Property State_TX', 'Property State_UT',
                'Property State_VA', 'Property State_VT', 'Property State_WA',
                'Property State_WI', 'Property State_WV', 'Property State_WY']

y = data["Delinquent"].astype(int)
X_df = data.drop(columns = ["Delinquent", "First Payment Date", "Postal Code"])
X_df = X_df.rename(columns={"Original Debt-to-Income (DTI) Ratio": "Debt-to-Income", "Original Loan-to-Value (LTV)": "Loan-to-Value", "Number of Borrowers": "# Borrowers",
                "Original Loan Term": "Loan Term", "Loan Purpose_N": "TPO not specified", "First Time Homebuyer Flag": "First Time Homebuyer", "Channel_R":"Retail",
                "Channel_C":"Correspondent", "Loan Purpose_P": "Primary Residence"
                })
X_df = X_df.rename(columns= { state: state.split(" ")[1] for state in  states})
#X_df = X_df.drop(columns = ["Unnamed: 0"])

X_cols = X_df.columns

X = X_df

train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.20, random_state=42)


def filter_columns_from_df(train_X, test_X, train_y, nr_important_features = 40):

    best_features = SelectKBest()
    fit = best_features.fit(train_X, train_y)

    feature_importance_list = list(abs(fit.scores_))
    column_importance = list(zip(feature_importance_list, train_X.columns))
    
    column_importance_sorted = sorted(column_importance, key = lambda tuple: tuple[0], reverse = True)[0:nr_important_features]
    columns_to_pick_from_X = [col_name for importance, col_name in column_importance_sorted]

    train_X_filtered_df = train_X[columns_to_pick_from_X]
    test_X_filtered_df = test_X[columns_to_pick_from_X]
    
    scores, features = zip(*column_importance_sorted)
    
    # plot
    fig, ax = plt.subplots(1,figsize=(5, 5))
    
    sns.barplot(x = list(features), y = list(scores),color = "#00549a" , ax = ax)
    
    plt.xticks(rotation=90)
    plt.subplots_adjust(bottom=0.35)
    fig.savefig(PATH_TO_PIPELINE +'3_Feature_Selection/anova.png', dpi=500)
    fig.show()

    return train_X_filtered_df, test_X_filtered_df


train_X_filtered, test_X_filtered = filter_columns_from_df(train_X, test_X, train_y, 20)


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(train_X_filtered, train_y, test_size=0.20, random_state=42)

from sklearn.linear_model import LogisticRegression
logistic_reg = LogisticRegression(max_iter = 1000, penalty='none', solver='lbfgs')
logistic_reg.fit(X_train, y_train)
scr = logistic_reg.score(X_test, y_test)
print(scr)