# feature selection based on most important features according to lasso


import matplotlib
import matplotlib.pyplot as plt
import matplotx
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from sklearn.linear_model import Lasso
from sklearn.linear_model import LassoCV
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn import preprocessing
PATH_TO_PIPELINE = "C:/Users/mot99/OneDrive/Dokumente/GitHub/freddiemac/Pipeline/"
data = pd.read_csv(PATH_TO_PIPELINE + r'3_Feature_Selection\encod_dataset_2010_20_eval.csv')
data = data.dropna()

states = ['Property State_AL', 'Property State_AR', 'Property State_AZ',
                'Property State_CA', 'Property State_CO', 'Property State_CT',
                'Property State_DC', 'Property State_DE', 'Property State_FL',
                'Property State_GA', 'Property State_HI', 'Property State_IA',
                'Property State_ID', 'Property State_IL', 'Property State_IN',
                'Property State_KS', 'Property State_KY', 'Property State_LA',
                'Property State_MA', 'Property State_MD', 'Property State_ME',
                'Property State_MI', 'Property State_MN', 'Property State_MO',
                'Property State_MS', 'Property State_MT', 'Property State_NC',
                'Property State_ND', 'Property State_NE', 'Property State_NH',
                'Property State_NJ', 'Property State_NM', 'Property State_NV',
                'Property State_NY', 'Property State_OH', 'Property State_OK',
                'Property State_OR', 'Property State_PA', 'Property State_PR',
                'Property State_RI', 'Property State_SC', 'Property State_SD',
                'Property State_TN', 'Property State_TX', 'Property State_UT',
                'Property State_VA', 'Property State_VT', 'Property State_WA',
                'Property State_WI', 'Property State_WV', 'Property State_WY']

y = data["Delinquent"].astype(int)
X_df = data.drop(columns = ["Delinquent"])
X_df = data.drop(columns = ["Delinquent", "First Payment Date", "Postal Code"])
X_df = X_df.rename(columns={"Original Debt-to-Income (DTI) Ratio": "Debt-to-Income", "Original Loan-to-Value (LTV)": "Loan-to-Value", "Number of Borrowers": "# Borrowers",
                "Original Loan Term": "Loan Term", "Loan Purpose_N": "TPO not specified", "First Time Homebuyer Flag": "First Time Homebuyer", "Channel_R":"Retail",
                "Channel_C":"Correspondent", "Loan Purpose_P": "Primary Residence"})
X_df = X_df.rename(columns= { state: state.split(" ")[1] for state in  states})

#X_df = X_df.drop(columns = ["Unnamed: 0"])
X_cols = X_df.columns
X = X_df

train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.20, random_state=42)





def filter_columns_from_df(train_X_df, test_X_df, train_y_df, nr_important_features = 10, classification = True):
    '''
    Do Lasso CV on the data. Filter the most "nr_important_features"-many important features.
    '''

    # 1. normalize the data
    combined_X = pd.concat([train_X_df, test_X_df], ignore_index=True)
    X_normalizer = preprocessing.StandardScaler().fit(combined_X)
    train_X_normalized = X_normalizer.transform(train_X_df)
    train_X_normalized_df = pd.DataFrame(train_X_normalized, columns = train_X_df.columns)
    test_X_normalized = X_normalizer.transform(test_X_df)
    test_X_normalized_df = pd.DataFrame(test_X_normalized, columns = test_X_df.columns)

    # 2. Do lasso CV to find the optimal regularization parameter
    print("\nFitting lasso cross validation to find good regularization parameter...")
    if classification:
        # do classification
        lasso_cv = LogisticRegressionCV(cv = 2, max_iter = 1000, penalty='l1', solver='liblinear')
        lasso_cv.fit(train_X_normalized_df, train_y_df)
        best_regularization_para = lasso_cv.C_[0]
    else:
        # do regression
        lasso_cv = LassoCV(cv=6, random_state=0, max_iter=1000) # for regression
        lasso_cv.fit(train_X_normalized_df, train_y_df)
        best_regularization_para = lasso_cv.alpha_

    # 3. Filter the most important features
    feature_importance_list = list(abs(lasso_cv.coef_[0]))
    column_importance = list(zip(feature_importance_list, train_X_normalized_df.columns))
    column_importance_sorted = sorted(column_importance, key = lambda tuple: tuple[0], reverse = True)[:nr_important_features]
    columns_to_pick_from_X = [col_name for importance, col_name in column_importance_sorted]
    
    train_X_filtered_df = train_X_df[columns_to_pick_from_X]
    test_X_filtered_df = test_X_df[columns_to_pick_from_X]


    # Train one last time on filtered data to find the optimal regularizatiion strength according to CV
    if classification:
        # do classification
        lasso_cv = LogisticRegressionCV(cv = 2, max_iter = 1000, penalty='l1', solver='liblinear')
        lasso_cv.fit(train_X_filtered_df, train_y_df)
        best_regularization_para = lasso_cv.C_[0]
    else:
        # do regression
        lasso_cv = LassoCV(cv=6, random_state=0, max_iter=1000).fit(train_X_normalized_df, train_y) # for regression
        lasso_cv.fit(train_X_filtered_df, train_y_df)
        best_regularization_para = lasso_cv.alpha_

    return train_X_filtered_df, test_X_filtered_df, best_regularization_para


#def choose_top_features_lasso(train_X, train_y, test_X, C, nr_of_features):
    '''
    This function filters from the given training data the most important (according to lasso) features and returns an adapted training set.

    Input:
        train_X (np.array or pd.dataframe):
        train_y (np.array or pd.dataframe):
        alpha (double): the parameter that defines in the lasso degression how important the regularization is
        nr_of_features (int): the amount of features that should be picked - depending on their importance according to lasso(alpha)

    Return:
        filtered_train_X (np.array): same amount of rows as train_X, but only the most important columns according to lasso
    '''
    
    # normalize the features
    # Combine the train and test X into one big X -> test_X are the last nr_test_rows rows
    combined_X = np.concatenate((train_X, test_X))
    '''
    X_normalizer = preprocessing.StandardScaler().fit(combined_X)
    train_X_normalized = X_normalizer.transform(train_X)
    y_normalizer = preprocessing.StandardScaler().fit(train_y)
    train_y_normalized = y_normalizer.transform(train_y)
    '''
    
    # train a lasso (logisitic) regression with the given data and parameters
    # lasso = Lasso(alpha=C, fit_intercept=False, max_iter = 20000) # for regression
    lasso = LogisticRegression(C = C, max_iter = 10000, penalty='l1', solver='liblinear') # for classification
    lasso.fit(train_X, train_y)
    # memorize the amount of entries that were contained in test_X
    nr_test_rows = len(test_X)
    
    # find the most important features according to lasso regression with parameters alphas
    importance_vec = lasso.coef_
    importance_sorted_list = sorted([(abs(coeff), index) for index, coeff in enumerate(importance_vec)], key=lambda x: x[0], reverse=True)
    important_feature_columns = [feature_name for (feature_importance, feature_name) in importance_sorted_list[:nr_of_features]]

    # filter the data by the features that should be chosen
    filtered_X = combined_X[:, important_feature_columns[0]].reshape((-1,1))
    for important_feature_id in range(1, len(important_feature_columns)):
        important_feature = important_feature_columns[important_feature_id]
        feature_data = combined_X[:, important_feature].reshape((-1,1))
        filtered_X = np.append(filtered_X,feature_data,axis=1)   
    
    # Split the dataframe into train and test data again
    train_X_feature_selected = filtered_X[:-nr_test_rows]
    test_X_feature_selected = filtered_X[-nr_test_rows:]
    
    return train_X_feature_selected, test_X_feature_selected


#def multiple_lassocv_for_feature_selection(train_X, test_X, train_y, list_lasso_cutoffs = [10]):
    '''
    Do multiple lasso cross-validations and with each one kick out the amount listed in list_lasso_cutoffs.
    
    train_X, test_X, train_y (np.array)
    list_lasso_cutoffs (decreasing towards the right)
    '''
    
    nr_of_lassocv = len(list_lasso_cutoffs)
    train_y = train_y.reshape(len(train_y))
    
    print("Do "+str(nr_of_lassocv)+" iterations of cutting best features from lasso:")
    
    
    # The rest of the code is only working on standardized data
    combined_X = np.concatenate((train_X, test_X))
    scaler = preprocessing.StandardScaler().fit(combined_X)
    train_X_normalized = scaler.transform(train_X)
    test_X_normalized = scaler.transform(test_X)

    # set the 
    train_X_subset = train_X_normalized
    test_X_subset = test_X_normalized
          
    for i in range(nr_of_lassocv): 
        # 1. get the best alpha according to cross validation of lasso
        print("\nFitting lasso cross validation to find optimal alpha...")
        lasso_cv = LogisticRegressionCV(cv = 2, max_iter = 10000, penalty='l1', solver='liblinear') # for classification
        #lasso_cv = LassoCV(cv=6, random_state=0, max_iter=20000).fit(train_X_subset_normalized, train_y) # for regression
        lasso_cv.fit(train_X_subset, train_y)
        # retrieve the best alpha according to cross validation
        lassocv_alpha = lasso_cv.C_[0]
        
        # 3. choose the best list_lasso_cutoffs[i] many features of a lasso with the found alpha
        nr_of_features = list_lasso_cutoffs[i]
        print("Choosing the most important "+str(nr_of_features)+" according to the alpha found by lasso cv...")
        train_X_subset, test_X_subset = choose_top_features_lasso(train_X = train_X_subset, train_y = train_y.reshape(-1, 1), test_X = test_X_subset, 
                                                                  C = lassocv_alpha, nr_of_features = nr_of_features)
     
    return train_X_subset, test_X_subset


# visualize the importance of different parameters when varying the regularization parameter of lasso (-> lasso traces)
def show_lasso_traces(train_X, train_y, n_alphas, lowest_log_alpha, highest_log_alpha, optimal_cv_para = None):
    '''
    Creates a pyplot of the different lasso traces - so the changing importance of the different features according to lasso regression
    The different alphas that will be used for the lasso regressions are on a log-scale between lowest_alpha and highest_alpha
    
    Input:
        train_X (np.array): 
        train_y (np.array): 
        n_alphas (int): amount of lasso regressions (at different alphas) that should be used to create the trace
        lowest_log_alpha (double): the lowest alpha that should be used for a lasso regression
        highest_log_alpha (double): the highest alpha that should be used for a lasso regression 
    '''
    lambdas = 1/np.logspace(lowest_log_alpha, highest_log_alpha, n_alphas)
    print("Computing Lasso Traces ...")
    coefs_lasso = []
    for l in lambdas:
        print("Fitting - Regularization Importance - 1/C = "+str(l))
        lasso_clf = LogisticRegression(C=l, max_iter = 10000, penalty='l1', solver='liblinear')
        lasso_clf.fit(train_X, train_y)
        print("Lasso Score: "+str(lasso_clf.score(train_X, train_y)))
        coefs_lasso.append(lasso_clf.coef_[0])
        # print(coefs)

    # TODO: visualization doesnt work! -> labels are missing
    fig, ax = plt.subplots(1,figsize=(6.5, 5))
    ax.plot(lambdas, coefs_lasso)#, label = train_X.columns)
    ax.set_xscale("log")
    # ax.set_xlim(ax.get_xlim()[::-1])  # reverse axis
    plt.xlabel("regularization")
    # plt.ylabel("importance")
    # plt.title("Lasso coefficients as a function of the regularization")

    if optimal_cv_para != None:
        plt.axvline(x = optimal_cv_para, color = 'b', label = 'CV optimal regularization')

    plt.legend(train_X.columns, loc='lower right')
    fig.savefig(PATH_TO_PIPELINE +'3_Feature_Selection/lasso.png', dpi=500)
    #matplotx.line_labels()  # line labels to the right
    #plt.axis("tight")
    plt.show()



# normalize the data 
X_normalizer = preprocessing.StandardScaler().fit(train_X)
train_X_normalized = X_normalizer.transform(train_X)
train_X_normalized_df = pd.DataFrame(train_X_normalized, columns = train_X.columns)
test_X_normalized = X_normalizer.transform(test_X)
test_X_normalized_df = pd.DataFrame(test_X_normalized, columns = test_X.columns)

#train_X_20, test_X_20 = multiple_lassocv_for_feature_selection(train_X_normalized_df.to_numpy(), test_X_normalized_df.to_numpy(), train_y.to_numpy(), [20])
train_X_filtered, test_X_filtered, cv_reg_para = filter_columns_from_df(train_X_df = train_X_normalized_df, test_X_df = test_X_normalized_df, train_y_df = train_y, nr_important_features = 10, classification = True)
print("Optimal regularizatioin strength according to CV: "+str(cv_reg_para))
show_lasso_traces(train_X = train_X_filtered, train_y = train_y, n_alphas = 20, lowest_log_alpha = -2, highest_log_alpha = 5, optimal_cv_para = cv_reg_para)
